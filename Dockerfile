ARG COMPOSER_VERSION=2
FROM composer:${COMPOSER_VERSION} as composer

ARG BASE_VERSION
FROM php:${BASE_VERSION} AS dev

LABEL org.opencontainers.image.authors="paul.bizouard@centralesupelec.fr"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

WORKDIR /app

RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | bash

RUN apt-get update && apt-get install -y --no-install-recommends \
    postgresql-client \
    libcurl4-gnutls-dev zlib1g-dev libicu-dev g++ libxml2-dev libpq-dev libonig-dev libzip-dev libldb-dev libpng-dev \
    software-properties-common \
    git unzip procps \
    locales \
    wget \
    libxrender1 \
    libc-dev make gcc autoconf libxslt-dev \
    symfony-cli

RUN pecl install redis && \
    docker-php-ext-enable redis && \
    docker-php-ext-install intl mbstring pdo pdo_pgsql zip bcmath sockets gd opcache soap xsl

# Install node
ARG NODE_VERSION
RUN if [ -n "$NODE_VERSION" ] ; then curl -sL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash - && apt-get update && apt-get install -y --no-install-recommends nodejs; fi

RUN pecl install xdebug && docker-php-ext-enable xdebug

# Install node
ARG RABBITMQ
RUN if [ -n "$RABBITMQ" ] ; then apt-get update && apt-get install -y --no-install-recommends librabbitmq-dev && pecl install amqp && docker-php-ext-enable amqp ; fi

RUN apt-get autoremove && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/*

RUN echo 'fr_FR.UTF-8 UTF-8' >> /etc/locale.gen && \
    locale-gen

RUN echo "date.timezone = Europe/Paris" >> /usr/local/etc/php/conf.d/timezone.ini && \
    echo 'memory_limit = 512M' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini && \
    echo 'expose_php = off' >> /usr/local/etc/php/conf.d/docker-php-expose.ini

RUN echo "access.log = /dev/null" >> /usr/local/etc/php-fpm.d/www.conf

COPY --from=composer /usr/bin/composer /usr/local/bin/composer

COPY symfony-entry-point.sh /
ENTRYPOINT  ["/bin/bash", "/symfony-entry-point.sh"]

CMD  ["php-fpm"]
